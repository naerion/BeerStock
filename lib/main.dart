import 'package:beer_stock/Product.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'ProductList.dart';
import 'login_page.dart';
import 'sign_in.dart';
import 'package:barcode_scan/barcode_scan.dart';

void main() {
  runApp(MaterialApp(
    title: 'Flutter Tutorial',
    home: LoginPage(),
  ));
}

class TutorialHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Scaffold is a layout for the major Material Components.
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: CircleAvatar(
            backgroundImage: NetworkImage(imageUrl),
          ),
          tooltip: 'Navigation menu',
          onPressed: null,
        ),
        title: Text('Example title'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            tooltip: 'Search',
            onPressed: null,
          ),
        ],
      ),
      // body is the majority of the screen.
      body: ProductList(),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Add', // used by assistive technologies
        child: Icon(Icons.crop_free),
        onPressed: () => {scan(context)},
      ),
    ));
  }

  void scan(BuildContext context) {
    BarcodeScanner.scan().then((res) {
      print(res);
      showModalBottomSheet(
          context: context,
          builder: (context) => Container(
                alignment: Alignment(0.0, 0.0),
                color: Colors.grey[900],
                height: 300,
                child: Center(
                    child: Scaffold(
                  body: addWidget(res),
                  floatingActionButton: FloatingActionButton(
                    tooltip: 'Save', // used by assistive technologies
                    child: Icon(Icons.save),
                    onPressed: () => {save()},
                  ),
                )),
              ));
    });
  }

  Widget addWidget(String ean) {
    return ProductWidget(ean: ean, quantity: 0);
  }

  void save() {}
}

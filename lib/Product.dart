import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ProductWidget extends StatefulWidget {
  ProductWidget({this.ean, this.quantity});

  final String ean;
  final int quantity;

  @override
  _ProductWidgetState createState() => _ProductWidgetState();
}

class _ProductWidgetState extends State<ProductWidget> {
  String _title = "";
  String _bottleQuantity = "33 cl";
  double _alcoholDegrees = 7.6;
  int _quantity;

  @override
  Widget build(BuildContext context) {
    if (_title == "") {
      // This is what we show while we're loading
      return Container(height: 0, width: 0);
    }
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 100,
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            elevation: 10,
            child: Row(
              children: <Widget>[
                Expanded(
                    child: ListTile(
                        leading:
                            CircleAvatar(child: Text(_quantity.toString())),
                        title: Text(_title,
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        subtitle: Row(
                          children: <Widget>[
                            Chip(
                              avatar: CircleAvatar(
                                child: Icon(Icons.call_merge),
                              ),
                              label: Text(_bottleQuantity,
                                  style: TextStyle(
                                      color: Colors.black.withOpacity(0.6))),
                            ),
                            Chip(
                              avatar: CircleAvatar(
                                child: Text('°'),
                              ),
                              label: Text(_alcoholDegrees.toString()),
                            ),
                          ],
                        ))),
                IconButton(
                  iconSize: 40,
                  icon: Icon(Icons.remove_circle_outline),
                  color: Colors.blue,
                  onPressed: () {
                    setState(() {
                      this._quantity--;
                    });
                  },
                ),
                IconButton(
                  iconSize: 40,
                  icon: Icon(Icons.add_circle_outline),
                  color: Colors.blue,
                  onPressed: () {
                    setState(() {
                      this._quantity++;
                    });
                  },
                ),
              ],
            )));
  }

  @override
  void initState() {
    super.initState();

    setState(() {
      this._quantity = widget.quantity;
    });

    _fetchData(widget.ean).then((res) {
      setState(() {
        _extractField(res);
      });
    });
  }

  _fetchData(String ean) async {
    var url = 'https://world.openfoodfacts.org/api/v0/product/' + ean;
    var response = await http.get(url);
    return json.decode(response.body);
  }

  _extractField(Map<String, dynamic> json) {
    _title = json['product']['product_name'];
    _alcoholDegrees = json['product']['nutriments']['alcohol_value'];
    _bottleQuantity = json['product']['quantity'];
  }
}
